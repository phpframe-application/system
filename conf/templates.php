<?php


return [
  'paths' => BASE_PATH.'app'.DS.'*'.DS.'views'.DS,
  'settings' => [
    'cache' => BASE_PATH.'tmp'.DS.'cache',
    'debug' => true
  ]
];
