<?php

// namespace PHPFrame;


global $BLUEPRINT;

/**
 *
 *
*/
$Application = PHPFrame\Core\Application::instance();

/**
 *
 *
*/
$Application->bind('blueprint', function() use($BLUEPRINT){
	return (object)$BLUEPRINT;
});

/**
 *
 *
*/
$Application->bind('configs', function() use($BLUEPRINT){

	$configs = [];
	foreach (glob($BLUEPRINT['configs'].DS.'*.php') as $config) {
		if(!is_dir($config)){
			$name = str_replace('.php', '', basename($config));
			$configs[$name] = require realpath($config);
		}
	}
	return json_decode(json_encode($configs));
});

/**
 *
 *
*/
$Application->bind('database', function() use($Application){
	$dbConfigs = $Application->get('configs')->database;
	return PHPFrame\Data\Database::connection($dbConfigs);
});

/**
 *
 *
*/
$Application->bind('request', function() use($Application){
	return PHPFrame\Http\Request::fromGlobals(
		$_POST, $_GET, $GLOBALS, $_SERVER, $_ENV, $_FILES
	);
});

/**
 *
 *
*/
$Application->bind('response', function() use($BLUEPRINT){
	return new PHPFrame\Http\Response();
});

/**
 *
 *
*/
$Application->bind('emitter', PHPFrame\Http\Emitter::class);

/**
 *
 *
*/
$Application->bind('routing', function() use($Application){
	$routes = glob($Application->get('blueprint')->routes);
	return new PHPFrame\Http\Routing($Application, $routes);
});

/**
 *
 *
*/
$Application->bind('template', function() use($Application){

	$templates = glob($Application->get('configs')->templates->paths);
	$settings  = (array)$Application->get('configs')->templates->settings;

	$loader = new PHPFrame\View\Template( $templates );
	return new PHPFrame\View\Viewer( $loader, $settings );

});

// echo $twig->render('index.html', array('name' => 'Fabien'));
/**
 *
 *
*/
return require $BLUEPRINT['runner'];
